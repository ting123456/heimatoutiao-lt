import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    alias: '/',
    component: () => import('../views/login/login.vue')
  },
  {
    path: '/home',
    component: () => import('../views/home/home.vue'),
    meta: {
      needTab: true
    },
    children: [
      {
        path: '/home/articledetails',
        component: () => import('../views/articledetails/articledetails.vue')
      },
      {
        path: '/home/index',
        component: () => import('../views/index/index.vue'),
        meta: {
          needTab: true
        }
      },
      {
        path: '/home/search',
        component: () => import('../views/search/search.vue'),
        meta: {
          needTab: true
        }
      },
      {
        path: '/home/my',
        component: () => import('../views/my/index.vue'),
        meta: {
          needTab: true
        }
      },
      {
        path: '/home/myinfo',
        component: () => import('../views/my/myinfo.vue')
      },
      {
        path: '/home/searchlist/:value?',
        component: () => import('../views/search/searchlist.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
