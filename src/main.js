import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css'
// 页面适配
import 'amfe-flexible'
// normalize
import 'normalize.css'
// 全局页面样式配置
import '@/style/base.css'
// iconfont导入
import '@/style/iconfont/iconfont.css'
// 注册全局的公共组件
import comm from '@/utils/comm'
Vue.use(comm)

// 使用 vant
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
