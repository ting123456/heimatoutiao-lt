import NavBar from '@/components/hmNavbar.vue'
export default function (Vue) {
  Vue.component(NavBar.name, NavBar)
}
