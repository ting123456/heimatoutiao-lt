function setlocal (key, value) {
  window.localStorage.setItem(key, value)
}
function getlocal (key) {
  return window.localStorage.getItem(key)
}
function removelocal (key) {
  window.localStorage.removeItem(key)
}
export { setlocal, getlocal, removelocal }
