import request from './request'
function searchHistory (data) {
  return request({
    url: '/v1_0/search/histories',
    needToken: true,
    data: data
  })
}
function clearAll (data) {
  return request({
    url: '/v1_0/search/histories',
    method: 'DELETE',
    needToken: true
  })
}
function thinklist (params) {
  return request({
    url: '/v1_0/suggestion',
    params
  })
}
function searchDetails (params) {
  return request({
    url: '/v1_0/search',
    needToken: true,
    params
  })
}
export { searchHistory, clearAll, thinklist, searchDetails }
