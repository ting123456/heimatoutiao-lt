import request from './request'

function my () {
  return request({
    url: '/v1_0/user',
    method: 'get',
    needToken: true
  })
}
function myinfo () {
  return request({
    url: '/v1_0/user/profile',
    method: 'get',
    needToken: true
  })
}
function upload (data) {
  return request({
    url: '/v1_0/user/photo',
    data,
    method: 'PATCH',
    needToken: true
  })
}
export { my, myinfo, upload }
