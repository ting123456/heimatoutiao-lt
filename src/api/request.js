import axios from 'axios'
import { getlocal, removelocal } from '../utils/local'
import store from '@/store'
import router from '@/router'
var jsonBig = require('json-bigint')

const request = axios.create({
  baseURL: process.env.VUE_APP_URL,

  // transformResponse 允许自定义原始的响应数据（字符串）
  transformResponse: [
    function (data) {
      try {
        // 如果转换成功则返回转换的数据结果
        return jsonBig.parse(data)
      } catch (err) {
        // 如果转换失败，则包装为统一数据格式并返回
        return {
          data
        }
      }
    }
  ]
})
// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // console.log(config)

    if (config.needToken) {
      config.headers.Authorization = `Bearer ${getlocal('token')}`
    }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    // console.log(response)
    if (
      response.status === 200 ||
      response.status === 204 ||
      response.status === 201
    ) {
      return response
    } else {
      // console.log('ddd')
    }
  },
  function (error) {
    // 对响应错误做点什么
    // console.dir(error)
    removelocal('token')
    removelocal('keywords')
    store.commit('setIslogin', false)
    router.push('/login')
    // if (error.response.status === 401 || error.response.status === 403) {
    //   // console.log(response.config.noError)
    //   removelocal('token')
    //   removelocal('keywords')
    //   store.commit('setIslogin', false)
    //   router.push('/login')
    //   // return Promise.reject(new Error(response.data.message))
    // }
    return Promise.reject(error)
  }
)
export default request
