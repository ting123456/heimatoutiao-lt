import request from './request'

// 用户频道
export function userChannels () {
  return request({
    url: '/v1_0/user/channels'
  })
}

// 频道文章列表
export function articles (params) {
  return request({
    url: '/v1_1/articles',
    params
  })
}

// 对文章不喜欢
export function dislikes () {
  return request({
    url: '/v1_0/article/dislikes',
    method: 'post'
  })
}

// 拉黑作者
export function blacklists () {
  return request({
    url: '/v1_0/user/blacklists',
    method: 'post'
  })
}

// 举报
export function reports () {
  return request({
    url: '/v1_0/article/reports',
    method: 'post'
  })
}

// 全部频道
export function allChannels () {
  return request({
    url: '/v1_0/channels'
  })
}

// 删除频道
export function delChannels (params) {
  return request({
    url: '/v1_0/user/channels',
    method: 'DELETE',
    params
  })
}
