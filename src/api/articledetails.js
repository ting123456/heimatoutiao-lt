import request from './request'

function articles (id) {
  return request({
    url: '/v1_0/articles/' + id,
    needToken: true
  })
}
function comments (params) {
  return request({
    url: '/v1_0/comments',
    needToken: true,
    params
  })
}
function writeComments (data) {
  return request({
    url: '/v1_0/comments',
    needToken: true,
    method: 'post',
    data
  })
}
// 取消关注
function delFollowings (id) {
  return request({
    url: '/v1_0/user/followings/' + id,
    needToken: true,
    method: 'DELETE'
  })
}

// 关注用户
function userFollowings (data) {
  return request({
    url: '/v1_0/user/followings',
    needToken: true,
    method: 'POST',
    data
  })
}

// 点赞文章
function articleLikings (data) {
  return request({
    url: '/v1_0/article/likings',
    needToken: true,
    method: 'POST',
    data
  })
}

// 取消点赞文章
function unarticleLikings (id) {
  return request({
    url: '/v1_0/article/likings/' + id,
    needToken: true,
    method: 'DELETE'
  })
}

// 不喜欢
function articleDislikes (data) {
  return request({
    url: '/v1_0/article/dislikes',
    needToken: true,
    method: 'POST',
    data
  })
}

// 收藏
function articleCollections (data) {
  return request({
    url: '/v1_0/article/collections',
    needToken: true,
    method: 'POST',
    data
  })
}

// 取消收藏
function DELarticleCollections (id) {
  return request({
    url: '/v1_0/article/collections/' + id,
    needToken: true,
    method: 'DELETE'
  })
}
export {
  articles,
  comments,
  writeComments,
  delFollowings,
  articleLikings,
  unarticleLikings,
  articleDislikes,
  userFollowings,
  articleCollections,
  DELarticleCollections
}
