import request from './request'

function auLogin (data) {
  return request({
    url: '/v1_0/authorizations',
    method: 'post',
    data: data
  })
}
export { auLogin }
